def sum(arr):
    result = 0;
    for num in str(arr):
        result += int(num);
    return result;

def max_sum__amount(arr):
    lst = [];
    for number in arr:
        lst.append(sum(number))
    max_value = max(lst)
    max_index = lst.index(max_value)
    return (max_index);
    
    
def isInt(value):
    try:
        int(value);
        return True;
    except ValueError:
        return False;
        

def main_menu():
    list = [];
    num = None;
    while True:
        num = input('Введите число: ')
        if isInt(num)==True and int(num) != 0:
            list.append(num);
        elif isInt(num)==False:
            print('Вы ввели не ЦЕЛОЕ число!')
            continue
        elif int(num) == 0:
            max_index = max_sum__amount(list);
            print(list[max_index])
            input("...")
            list = []


if __name__ == '__main__':
    main_menu()